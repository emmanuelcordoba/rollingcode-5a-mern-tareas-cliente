import React, { Fragment, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Aside from './layout/Aside';
import Header from './layout/Header';
import NuevaTarea from './tareas/NuevaTarea';
import ListadoTareas from './tareas/ListadoTareas';

export default function Home({ usuarioAuth, token, setToken, etiquetas, setEtiquetas, etiquetaSel, setEtiquetaSel, tareasEtiqueta, setTareasEtiqueta, paginacionTareas, traerTareasEtiqueta }) {
    
    const [tareaMod, setTareaMod] = useState(null);
    
    return (
        <Container className="px-0" fluid>
            <Row className="mx-0">
                <Col xs={12} md={3} className="bg-light border-right">
                    <Aside 
                        token={token}
                        etiquetas={etiquetas}
                        setEtiquetas={setEtiquetas}
                        etiquetaSel={etiquetaSel}
                        setEtiquetaSel={setEtiquetaSel}
                        tareaMod={tareaMod}
                    />
                </Col>
                <Col xs={12} md={9} className="px-0">
                    <Header
                        usuarioAuth={usuarioAuth}
                        setToken={setToken}
                    />
                    <main>
                        <Container>
                            {
                                etiquetaSel ? 
                                <Fragment>
                                    <Row>
                                        <NuevaTarea 
                                            token={token}
                                            etiquetaSel={etiquetaSel}
                                            tareasEtiqueta={tareasEtiqueta}
                                            setTareasEtiqueta={setTareasEtiqueta}
                                            tareaMod={tareaMod}
                                            setTareaMod={setTareaMod}
                                        />
                                    </Row>
                                    <Row>
                                        <ListadoTareas 
                                            token={token}
                                            etiquetaSel={etiquetaSel} 
                                            setEtiquetaSel={setEtiquetaSel}
                                            tareasEtiqueta={tareasEtiqueta}
                                            setTareasEtiqueta={setTareasEtiqueta}
                                            tareaMod={tareaMod}
                                            setTareaMod={setTareaMod}
                                            paginacionTareas={paginacionTareas}
                                            traerTareasEtiqueta={traerTareasEtiqueta}
                                        />
                                    </Row>
                                </Fragment>
                                : <Row><Col><h3 className="text-center mt-3">Selecione una etiqueta</h3></Col></Row>
                            }
                        </Container>
                    </main>
                </Col>
            </Row>
        </Container>
    )
}
