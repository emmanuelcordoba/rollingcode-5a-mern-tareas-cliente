import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';

export default function Login({ setUsuarioAuth, setToken, setAutenticado }) {
    
    const [usuario, setUsuario] = useState({
        email: '',
        password: ''
    });

    // Extraemos
    const { email, password } = usuario;

    // Cuando cambian los campos
    const onChangeUsuario = e => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }


    // Cuando el usuario quiere iniciar sesion
    const onSubmitLogin = async e => {
        e.preventDefault();
        console.log('Login!');

        if(email.trim() === '' || password.trim() === ''){
            alert('Todos los campos son obligatorios');
            return;
        }

        const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/api/auth/',{
            method: 'POST',
            body: JSON.stringify(usuario),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        //console.log(solicitud);
        const respuesta = await solicitud.json();
        //console.log(respuesta);

        if(solicitud.ok){
            // Resetear el formulario
            setUsuario({
                email: '',
                password: ''
            });

            // Setear usuario autenticado
            setUsuarioAuth(respuesta.usuario);

            // Setear autenticado
            setAutenticado(true);

            // Setear el token
            setToken(respuesta.token);

        } else {
            alert(respuesta.msg);
        }
    }
    
    return (
        <Container>
            <Row className="mt-5">
                <Col xs={12} sm={8} md={6} className="mx-auto">
                    <Card bg="light">
                        <Card.Header>Login</Card.Header>
                        <Card.Body>
                            <Form onSubmit={onSubmitLogin}>
                                <Form.Group controlId="formLoginEmail">
                                    <Form.Label>
                                        Email
                                    </Form.Label>
                                    <Form.Control 
                                        type="email"
                                        name="email"
                                        placeholder="Ingrese su email"
                                        onChange={onChangeUsuario}
                                        value={email}
                                    />
                                </Form.Group>
        
                                <Form.Group controlId="formLoginPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control 
                                        type="password"
                                        name="password"
                                        placeholder="Ingrese su password"
                                        onChange={onChangeUsuario}
                                        value={password}
                                    />
                                </Form.Group>
                                <Button 
                                    className="mr-3"
                                    variant="primary"
                                    type="submit">Iniciar Sesión
                                </Button>
                                <Link to="/registro">Crear usuario</Link>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>                
            </Row>
        </Container>
    )
}
