import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Form, Button, Card } from 'react-bootstrap';

export default function Registro({ setUsuarioAuth, setAutenticado, setToken}) {
    
    const [usuario, setUsuario] = useState({
        nombre: '',
        email: '',
        password: '',
        passwordconfirm: ''
    });

    const { nombre, email, password, passwordconfirm } = usuario;

    // Cuando cambian los campos
    const onChangeUsuario = e => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }


    // Cuando el usuario quiere registrarse
    const onSubmitRegistro = async e => {
        e.preventDefault();
        console.log('Registro!');

        if(nombre.trim() === '' || email.trim() === '' || password.trim() === '' || passwordconfirm.trim() === '' ){
            alert('Todos los campos son obligatorios');
            return;
        }

        const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/api/usuarios/',{
            method: 'POST',
            body: JSON.stringify(usuario),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        //console.log(solicitud);
        const respuesta = await solicitud.json();
        //console.log(respuesta);

        if(solicitud.ok){
            // Resetear el formulario
            setUsuario({
                nombre: '',
                email: '',
                password: '',
                passwordconfirm: ''
            });

            alert('Usuario registrado correctamente');

            // Setear usuario autenticado
            setUsuarioAuth(respuesta.usuario);

            // Setear autenticado
            setAutenticado(true);

            // Setear el token
            setToken(respuesta.token);

        } else {
            alert(respuesta.msg);
        }    
    }

    return (
        <Container>
            <Row className="mt-5">
                <Col xs={12} sm={8} md={6} className="mx-auto">
                    <Card bg="light">
                        <Card.Header>Registro de Usuario</Card.Header>
                        <Card.Body>
                            <Form onSubmit={onSubmitRegistro}>
                                <Form.Group controlId="formRegistroNombre">
                                    <Form.Label>
                                        Nombre
                                    </Form.Label>
                                    <Form.Control 
                                        type="text"
                                        name="nombre"
                                        placeholder="Ingrese su nombre"
                                        onChange={onChangeUsuario}
                                        value={nombre}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group controlId="formRegistroEmail">
                                    <Form.Label>
                                        Email
                                    </Form.Label>
                                    <Form.Control 
                                        type="email"
                                        name="email"
                                        placeholder="Ingrese su email"
                                        onChange={onChangeUsuario}
                                        value={email}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group controlId="formRegistroPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control 
                                        type="password"
                                        name="password"
                                        placeholder="Ingrese su password"
                                        onChange={onChangeUsuario}
                                        value={password}
                                        required
                                    />
                                </Form.Group>
                                <Form.Group controlId="formRegistroPasswordconfirm">
                                    <Form.Label>Confirmar Password</Form.Label>
                                    <Form.Control 
                                        type="password"
                                        name="passwordconfirm"
                                        placeholder="Ingrese su password de nuevo"
                                        onChange={onChangeUsuario}
                                        value={passwordconfirm}
                                        required
                                    />
                                </Form.Group>
                                <Button 
                                    className="mr-3"
                                    variant="primary"
                                    type="submit">Registrarme
                                </Button>
                                <Link to="/login">Iniciar sesión</Link>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>                
            </Row>
        </Container>
    )
}
