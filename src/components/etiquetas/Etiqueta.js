import React from 'react';
import { ListGroup } from 'react-bootstrap';

export default function Etiqueta({ etiqueta, etiquetaSel, setEtiquetaSel, tareaMod }) {
    
    const onClickEtiqueta = () => {
        if(!tareaMod){
            setEtiquetaSel(etiqueta);
        } else {
            alert('Termine de editar la tarea seleccionada.');
        }
    }
    
    return (
        <ListGroup.Item
            as="button"
            className="list-group-item-action"
            onClick={onClickEtiqueta}
            active={ etiquetaSel && etiquetaSel._id == etiqueta._id }
        >{ etiqueta.nombre }</ListGroup.Item>
    )
}
