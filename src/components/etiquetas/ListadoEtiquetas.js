import React from 'react';
import Etiqueta from './Etiqueta';
import { ListGroup } from 'react-bootstrap';

export default function ListadoEtiquetas({ etiquetas, etiquetaSel, setEtiquetaSel, tareaMod }) {

    return (
        <ListGroup className="list-group-flush">
            {
                etiquetas.map( etiqueta => (
                    <Etiqueta 
                        key={etiqueta._id}
                        etiqueta={etiqueta}
                        etiquetaSel={etiquetaSel}
                        setEtiquetaSel={setEtiquetaSel}
                        tareaMod={tareaMod}
                    />
                ))
            }
        </ListGroup>
    )
}
