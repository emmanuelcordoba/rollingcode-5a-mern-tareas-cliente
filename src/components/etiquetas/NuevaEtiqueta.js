import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function NuevaEtiqueta({ token, etiquetas, setEtiquetas }) {
    
    const [etiqueta, setEtiqueta] = useState({
        nombre: ''
    });

    // Extraemos de la etiqueta
    const { nombre } = etiqueta

    // Cuando cambian los campos
    const onChangeEtiqueta = e => {
        setEtiqueta({
            ...etiqueta,
            [e.target.name]: e.target.value
        })
    }


    // Cuando el usuario quiere iniciar sesion
    const onSubmitNuevaEtiqueta = async e => {
        e.preventDefault();
        console.log('Nueva Etiqueta!');

        if(nombre.trim() === ''){
            alert('Ingrese el nombre de la etiqueta');
            return;
        }

        const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/api/etiquetas',{
            method: 'POST',
            body: JSON.stringify(etiqueta),
            headers: {
                'Content-Type': 'application/json',
                'x-auth-token': token
            }
        })

        console.log(solicitud);
        const respuesta = await solicitud.json();
        console.log(respuesta);

        if(solicitud.ok){
            // Restauramos el formulario
            setEtiqueta({
                nombre: ''
            });

            // Agregamos la etiqueta
            setEtiquetas([ ...etiquetas, respuesta.etiqueta ])
        } else {
            alert(respuesta.msg);
        }
    
    }
    
    return (
        <Form className="mb-5" onSubmit={onSubmitNuevaEtiqueta}>
            <Form.Group controlId="formEtiquetaNombre">
                <Form.Label>
                    Etiqueta
                </Form.Label>
                <Form.Control 
                    type="text"
                    name="nombre"
                    placeholder="Nombre de la etiqueta"
                    onChange={onChangeEtiqueta}
                    value={nombre}
                />
            </Form.Group>
            <Button 
                variant="primary"
                type="submit"
                block
            >Agregar Etiqueta
            </Button>
        </Form>
    )
}
