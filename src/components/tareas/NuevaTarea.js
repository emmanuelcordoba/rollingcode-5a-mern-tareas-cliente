import React, { useState, useEffect } from 'react';
import { Card, Form, Button, Row, Col, Image } from 'react-bootstrap';
 
export default function NuevaTarea({ token, etiquetaSel, tareasEtiqueta, setTareasEtiqueta, tareaMod, setTareaMod }) {

    const [tarea, setTarea] = useState({
        nombre: '',
        descripcion: '',
        imagen: null
    });

    useEffect(() => {
        if(tareaMod){
            setTarea(tareaMod);
        }
    }, [tareaMod])

    // Extraer de la tarea
    const { nombre, descripcion, imagen } = tarea;

    // Cuando hay cambios en el formulario
    const onChangeTarea = e => {
        setTarea({
            ...tarea,
            [e.target.name]: e.target.value
        })
    }

    // Cuando hay un cambio en el campo imagen
    const onChangeImagen = e => {
        
        if(e.target.files.length){
            if(e.target.files[0].size > 4194304){
                // 5242880 = 5MB
                // 4194304 = 4MB
                e.target.value=null;
                alert('La imagen es demasiado grande');
                setTarea({
                    ...tarea,
                    imagen: null
                });
                return;
            } 
            let reader = new FileReader();
            reader.readAsDataURL(e.target.files[0]);
            reader.onloadend = () => {
                setTarea({
                    ...tarea,
                    imagen: reader.result
                });
            }
        } else {
            setTarea({
                ...tarea,
                imagen: null
            });
        }
    }
    
    // Cuando el usuario crea una tarea
    const onSubmitNuevaTarea = async e => {
        e.preventDefault();
        console.log('Nueva Tarea!');
    
        if(nombre.trim() === '' || descripcion.trim() === ''){
            alert('Ingrese los campos obligatorios');
        }

        if(!tareaMod){
            tarea.etiqueta = etiquetaSel._id;
            //console.log(tarea);
        
            // Agregar tarea        
            const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/api/tareas',
                {
                    method: 'POST',
                    body: JSON.stringify(tarea),
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': token
                    }
                }
            );
    
            //console.log(solicitud);
            const respuesta = await solicitud.json();
            //console.log(respuesta);
    
            if(solicitud.ok){
                // Resetear el fomulario
                setTarea({
                    nombre: '',
                    descripcion: '',
                    imagen: null
                });
                document.getElementById('formNuevoTareaImagen').value = "";
    
                // Agregar tarea nueva
                setTareasEtiqueta([
                    ...tareasEtiqueta,
                    respuesta.tarea
                ]);
            } else {
                console.log(respuesta.msg)
            }

        } else {

            // Modificar tarea        
            const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/api/tareas/'+tareaMod._id,
                {
                    method: 'PUT',
                    body: JSON.stringify(tarea),
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': token
                    }
                }
            );
    
            console.log(solicitud);
            const respuesta = await solicitud.json();
            console.log(respuesta);
    
            if(solicitud.ok){
                // Resetear el fomulario
                setTarea({
                    nombre: '',
                    descripcion: '',
                    imagen: null
                });
                document.getElementById('formNuevoTareaImagen').value = "";
    
                // Modificar tarea
                setTareasEtiqueta([
                    ...tareasEtiqueta.filter(t => t._id !== tareaMod._id),
                    respuesta.tarea
                ]);
                setTareaMod(null);
            } else {
                console.log(respuesta.msg)
            }
        }

    }

    

    return (
        <Card bg="light" className="w-50 mx-auto mt-3">
            <Card.Body>
                <Form onSubmit={onSubmitNuevaTarea}>
                    <Form.Group as={Row} controlId="formNuevaTareaNombre">
                        <Form.Label 
                            column 
                            xs="3"
                            className="pr-0"
                        >Nombre</Form.Label>
                        <Col xs="9">
                            <Form.Control 
                                type="text"
                                name="nombre"
                                placeholder="Ingrese el nombre"
                                onChange={onChangeTarea}
                                value={nombre}
                                required
                            />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formNuevoTareaDescripcion">
                        <Form.Label 
                            column 
                            sm="3" 
                            className="pr-0"
                        >Descripción</Form.Label>
                        <Col sm="9">
                            <Form.Control 
                                as="textarea" 
                                rows="3"
                                name="descripcion"
                                placeholder="Ingrese la descripción"
                                onChange={onChangeTarea}
                                value={descripcion}
                                required
                            />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formNuevoTareaImagen">
                        <Form.Label 
                            column 
                            sm="3" 
                            className="pr-0"
                        >Imágen</Form.Label>
                        <Col sm="9">
                            <Form.File 
                                label="Seleccione una imágen"
                                name="imagen"
                                onChange={onChangeImagen}
                                custom
                            />
                        </Col>
                    </Form.Group>
                    {
                        imagen ? 
                        <Row className="mb-3">
                            <Col sm={{ span:9, offset:3 }}>
                                <Image src={imagen} thumbnail/>
                            </Col>
                        </Row> : null
                    }
                    <Button
                        type="submit"
                        variant="primary"
                        className="float-right"
                    >{ tareaMod ? 'Modificar' : 'Agregar' }</Button>
                </Form>
            </Card.Body>
        </Card>        
    )
}
