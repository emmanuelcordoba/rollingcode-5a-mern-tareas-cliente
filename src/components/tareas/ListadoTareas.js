import React, { Fragment } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import Tarea from './Tarea';
import Paginacion from '../layout/Paginacion';

export default function ListadoTareas({ token, etiquetaSel, setEtiquetaSel, tareasEtiqueta, setTareasEtiqueta, tareaMod, setTareaMod, paginacionTareas, traerTareasEtiqueta }) {


    const borrarEtiqueta = async () => {
        if(window.confirm('¿Está seguro que desea borrar la etiqueta?')){
            const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/api/etiquetas/'+etiquetaSel._id,
                {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': token
                    }
                }
            );
    
            //console.log(solicitud);
            const respuesta = await solicitud.json();
            //console.log(respuesta);
    
            if(solicitud.ok){
                console.log('Etiqueta borrada.');    
                // Quitar etiqueta seleccionada
                setEtiquetaSel(null);
            } else {
                console.log(respuesta.msg)
            }
        }
    }
  

    return(
        <Fragment>
            <h4 className="w-100 mt-3 text-center">Etiqueta: { etiquetaSel.nombre }</h4>
            <Container className="px-0" fluid>
                <Row className="mx-0 mb-3">
                    <Button 
                        variant="danger" 
                        size="sm" 
                        className="mx-auto"
                        onClick={borrarEtiqueta}
                    >Borrar</Button>
                </Row>
                <Row className="mx-0 row-cols-3">
                    {
                        tareasEtiqueta.map(tarea => (
                            <Col key={tarea._id}>
                                <Tarea                                    
                                    token={token}
                                    tarea={tarea}
                                    tareasEtiqueta={tareasEtiqueta}
                                    setTareasEtiqueta={setTareasEtiqueta}
                                    tareaMod={tareaMod}
                                    setTareaMod={setTareaMod}
                                />
                            </Col>
                        ))
                    }
                </Row>
                <Row>
                    <Col className="mt-2 mx-3">
                        {
                            paginacionTareas ? 
                            <Paginacion 
                                paginacionData={paginacionTareas} 
                                funcionGetData={traerTareasEtiqueta}
                            />
                            : null
                        }
                    </Col>                    
                </Row>
            </Container>
        </Fragment>
    )
}