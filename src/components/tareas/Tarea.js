import React from 'react';
import { Card, ButtonGroup, Button } from 'react-bootstrap';
 
export default function Tarea({ token, tarea, tareasEtiqueta, setTareasEtiqueta, tareaMod, setTareaMod }) {
    
    const cambiarEstadoTarea = async () => {
        tarea.finalizada = !tarea.finalizada;

        const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/api/tareas/'+tarea._id,
            {
                method: 'PUT',
                body: JSON.stringify(tarea),
                headers: {
                    'Content-Type': 'application/json',
                    'x-auth-token': token
                }
            }
        );

        //console.log(solicitud);
        const respuesta = await solicitud.json();
        //console.log(respuesta);

        if(solicitud.ok){
            // console.log('Tarea modificada.');

            // Agregar tarea modificada
            setTareasEtiqueta([
                ...tareasEtiqueta.filter(t => t._id !== tarea._id),
                respuesta.tarea
            ]);
        } else {
            console.log(respuesta.msg)
        }
    }

    const borrarTarea = async () => {
        if(window.confirm('¿Esta seguro que desea borrar la tarea?')){
            const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/api/tareas/'+tarea._id,
                {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                        'x-auth-token': token
                    }
                }
            );
    
            //console.log(solicitud);
            const respuesta = await solicitud.json();
            //console.log(respuesta);
    
            if(solicitud.ok){
                // console.log('Tarea borrada.');    
                // Borrar tarea
                setTareasEtiqueta([...tareasEtiqueta.filter(t => t._id !== tarea._id)]);
            } else {
                console.log(respuesta.msg)
            }

        }
    }

    const modificarTarea = () => {
        if(!tareaMod){
            setTareaMod(tarea);
        } else{
            alert('Termine de editar la tarea seleccionada.');
        }
    }
    
    return (
        <Card>
            <Card.Img variant="top" src={ tarea.imagen ? tarea.imagen : 'https://via.placeholder.com/300px300' } />
            <Card.Body>
                <Card.Title>{ tarea.nombre }</Card.Title>
                <Card.Text>
                    { tarea.descripcion }
                </Card.Text>
                <ButtonGroup size="sm">
                    {
                        tarea.finalizada ? 
                        <Button variant="success" onClick={cambiarEstadoTarea}>Finalizada</Button>
                        : <Button variant="secondary" onClick={cambiarEstadoTarea}>Pendiente</Button>
                    }                    
                    <Button variant="warning" onClick={modificarTarea} >Editar</Button>
                    <Button variant="danger" onClick={borrarTarea}>Borrar</Button>
                </ButtonGroup>
            </Card.Body>
        </Card>
    )
}
