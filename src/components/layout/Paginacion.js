import React from 'react';
import { Pagination } from 'react-bootstrap';

export default function Paginacion({ paginacionData, funcionGetData }) {
    
    const {
            hasNextPage, 
            hasPrevPage, 
            limit, 
            nextPage, 
            page, 
            pagingCounter,
            prevPage, 
            totalPages
        } = paginacionData;
    
    return (
        <Pagination>
            <Pagination.First onClick={() => funcionGetData(1)} />
            {
                hasPrevPage ?
                <Pagination.Prev onClick={() => funcionGetData(prevPage)}/>
                : <Pagination.Prev disabled/>
            }            
            <Pagination.Item active>{ page }</Pagination.Item>
            {
                hasNextPage ?
                <Pagination.Next onClick={() => funcionGetData(nextPage)}/>
                : <Pagination.Next disabled/>
            }
            <Pagination.Last onClick={() => funcionGetData(totalPages)} />
        </Pagination>
    )
}
