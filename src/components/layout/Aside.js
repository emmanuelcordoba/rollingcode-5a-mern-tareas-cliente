import React from 'react';
import NuevaEtiqueta from '../etiquetas/NuevaEtiqueta';
import ListadoEtiquetas from '../etiquetas/ListadoEtiquetas';

export default function Aside({ token, etiquetas, setEtiquetas, etiquetaSel, setEtiquetaSel, tareaMod }) {
    return (
        <aside className="p-2 h-100">
            <h3 className="mb-3 text-center">MERN Tareas</h3>
            <NuevaEtiqueta 
                token={token}
                etiquetas={etiquetas}
                setEtiquetas={setEtiquetas}
            />
            <h5 className="text-center">Tus Etiquetas</h5>
            <ListadoEtiquetas 
                etiquetas={etiquetas}
                etiquetaSel={etiquetaSel}
                setEtiquetaSel={setEtiquetaSel}
                tareaMod={tareaMod}
            />
        </aside>
    )
}
