import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';

export default function Header({ usuarioAuth, setToken }) {
    
    const onClickLogout = () => {
        if(window.confirm('¿Está seguro que desea cerrar sesión?')){
            setToken(null);
        }
    }
    
    return (
        <Navbar bg="dark" variant="dark">            
            <Navbar.Brand>
                <Navbar.Text className="mr-1">Hola</Navbar.Text>
                { usuarioAuth.nombre }
            </Navbar.Brand>
            <Nav className="ml-auto">
                <Nav.Link onClick={onClickLogout}>Cerrar Sesión</Nav.Link>
            </Nav>
        </Navbar>
    )
}
