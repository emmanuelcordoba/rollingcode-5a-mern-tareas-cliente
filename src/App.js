import React, { useState, useEffect } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Home from "./components/Home";
import Login from "./components/auth/Login";
import Registro from "./components/auth/Registro";

function App() {
  const [usuarioAuth, setUsuarioAuth] = useState(JSON.parse(localStorage.getItem('usuario')) || null);
  const [token, setToken] = useState(localStorage.getItem('token') || null);
  const [autenticado, setAutenticado] = useState(localStorage.getItem('token') ? true : false);

  const [etiquetas, setEtiquetas] = useState([]);
  const [etiquetaSel, setEtiquetaSel] = useState(null);

  const [tareasEtiqueta, setTareasEtiqueta] = useState([]);
  const [paginacionTareas, setPaginacionTareas] = useState(null);
  
  
  useEffect( () => {
    if(token){
      localStorage.setItem('token',token);

      traerEtiquetas();
      
    } else {
      localStorage.removeItem('token');
      setUsuarioAuth(null);
      setAutenticado(false);
    }
  }, [token]);

  useEffect(() => {
    if(usuarioAuth){
      localStorage.setItem('usuario',JSON.stringify(usuarioAuth));
    } else {
      localStorage.removeItem('usuario');
    }
  }, [usuarioAuth]);

  useEffect( () => {
    if(etiquetaSel){
      traerTareasEtiqueta();      
    } else {
      setTareasEtiqueta([]);
      traerEtiquetas();
    }
  }, [etiquetaSel]);

  const traerEtiquetas = async () => {
    const solicitud = await fetch(process.env.REACT_APP_BACKEND_URL+'/api/etiquetas/', {
      headers: {
          'Content-Type': 'application/json',
          'x-auth-token': token
      }    
    });
    //console.log(solicitud);
    const respuesta = await solicitud.json();
    //console.log(respuesta);
  
    if(solicitud.ok){
        setEtiquetas(respuesta.etiquetas);
    } else {
        alert(respuesta.msg);
    }
  }

  const traerTareasEtiqueta = async (page = null) => {
    let url = process.env.REACT_APP_BACKEND_URL+'/api/etiquetas/'+etiquetaSel._id+'/tareas';
    if(page){
      url += '?page='+page;
    }
    const solicitud = await fetch(url, {
      headers: {
          'Content-Type': 'application/json',
          'x-auth-token': token
      }    
    });
    //console.log(solicitud);
    const respuesta = await solicitud.json();
    //console.log(respuesta);
  
    if(solicitud.ok){
        setTareasEtiqueta(respuesta.tareas);
        delete respuesta.tareas;
        setPaginacionTareas(respuesta);
    } else {
        alert(respuesta.msg);
    }
  }

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          {
            autenticado ? 
            <Home 
              usuarioAuth={usuarioAuth}
              token={token}
              setToken={setToken}
              etiquetas={etiquetas}
              setEtiquetas={setEtiquetas}
              etiquetaSel={etiquetaSel}
              setEtiquetaSel={setEtiquetaSel}
              tareasEtiqueta={tareasEtiqueta}
              setTareasEtiqueta={setTareasEtiqueta}
              paginacionTareas={paginacionTareas}
              traerTareasEtiqueta={traerTareasEtiqueta}
            />
            : <Redirect to="/login" />
          }
        </Route>
        <Route exact path="/login">
          {
            !autenticado ? 
            <Login 
              setUsuarioAuth={setUsuarioAuth}
              setToken={setToken}
              setAutenticado={setAutenticado}
            />
            : <Redirect to="/" />
          }
        </Route>
        <Route exact path="/registro">
          {
            !autenticado ? 
            <Registro 
              setUsuarioAuth={setUsuarioAuth}
              setToken={setToken}
              setAutenticado={setAutenticado}
            />
            : <Redirect to="/" />
          }
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
